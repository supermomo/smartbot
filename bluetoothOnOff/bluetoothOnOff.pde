import communication.*;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.widget.Toast;
import android.view.Gravity;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.graphics.drawable.Drawable;
import android.app.AlertDialog;
import android.widget.LinearLayout;
import android.widget.Button;
import android.content.DialogInterface;

import android.os.PowerManager;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import java.lang.Math;
import android.bluetooth.BluetoothSocket;
import java.io.*;
import java.util.UUID;


boolean isMousePressed;
boolean isMovedFromJoy;

int screenWidth;
int screenHeight;

int offset = 300;
int borderX;
int borderY;

PVector joyPos;
PVector joyZero;

//Battery widget
boolean batteryVisible;

// Joystick 
final int circleWidth = 150;
final int joyWidth = 100;

// Nitro buttton
final int nitroButtonWidth = 125;
final int nitroOffset = 20;
final int maxNitroTimer = 2000;
int boostLaunchDate; 
boolean isBoosted;

PFont f;

BluetoothSocket mmSocket;
boolean foundDevice=false;
BluetoothDevice botBluetooth;

//discovered devices list
ArrayList<BluetoothDevice> devices;
SubMenu devicesList;
int listSize;
final int listIdOffset = 10;

//Get the default Bluetooth adapter
BluetoothAdapter bluetooth = BluetoothAdapter.getDefaultAdapter();
//Bluetooth read/write stream

//Screen lock
PowerManager.WakeLock wl;

/* Create a Broadcast Receiver that will later be used to 
 receive the names of Bluetooth devices in range. */
BroadcastReceiver myDiscoverer = new myOwnBroadcastReceiver();

boolean bluetoothEnabledAtStart = true;

ComModule smartCom;
PImage background = null;
PImage joystickBack;
PImage joystick;
PImage batteryImage;
PImage acceleratorImage;
PImage throttleImage;
PImage nitroButtonOn;
PImage nitroButtonOff;

PVector lightSource;

final float lightRatio = 0.8f;
final int shadowAlpha = 50;
int camWidth = 32;//taille de base : 160*120
int camHeight = 24;

// IHM Variables
//GROUPS
final int OPTIONS = 0;
final int DEVICES_MENU = 1;
//IDs
final int EXIT_ITEM = 1;
final int TOGGLE_BATTERY = 2;
final int DEVICES = 3;
final int REFRESH_DEVICES = 4;
final int TRIMER = 5;
final int TOGGLE_SMART = 6;

@Override
public boolean onCreateOptionsMenu(Menu menu) {
  Drawable exitIcon = Drawable.createFromPath("data/power.png");
  print("icon :" + exitIcon);
  menu.add(OPTIONS, EXIT_ITEM, Menu.NONE, "Exit").setIcon(Drawable.createFromPath("data/power.png"));
  menu.add(OPTIONS, TOGGLE_BATTERY, Menu.NONE, "Toggle battery status").setIcon(Drawable.createFromPath("data/battery.png"));
  devicesList = menu.addSubMenu(OPTIONS, DEVICES, Menu.NONE, "Select device");
  devicesList.setIcon(Drawable.createFromPath("data/bluetooth.png"));
  menu.add(OPTIONS, REFRESH_DEVICES, Menu.NONE, "Acquire Bluetooth").setIcon(Drawable.createFromPath("data/sync.png"));
  menu.add(OPTIONS, TRIMER, Menu.NONE, "Trimer");
  menu.add(OPTIONS, TOGGLE_SMART, Menu.NONE, "Toggle Smart Mode");
  return super.onCreateOptionsMenu(menu);
}

@Override
public boolean onOptionsItemSelected(MenuItem item) {
  // Handle item selection
  switch (item.getItemId()) {
  case EXIT_ITEM:
    print("Exiting ...");
    if (wl.isHeld())
      wl.release();
    exit();
    return true;
  case TOGGLE_BATTERY:
    batteryVisible = !batteryVisible;
    return true;
  case DEVICES:
    print("size " + devices.size() + " size menu " + devicesList.size());
    if (devices.size() == 0 || devicesList.size() == 0) {
      ToastMaster("no device found");
    }
    return true;
  case REFRESH_DEVICES:
    ToastMaster("refreshing device list");
    if (!bluetooth.isDiscovering()) {
      devices.clear();
      devicesList.clear();
      listSize = 0;
      foundDevice = false;
      if (smartCom != null) {
        smartCom.stopThreads();
        smartCom = null;
      }
      bluetooth.startDiscovery();
    }
    return true;
  case TRIMER:
    showTrimerDialog();
    return true;
  case TOGGLE_SMART:
    if (smartCom == null)
      ToastMaster("No SmartBot paired");
    else
       smartCom.toggleSmartMode();
    return true;
  default:
    if (item.getItemId() >= listIdOffset) {
      // device changed > GWENNNN !!!
      print (item.getItemId()-listIdOffset + " selected");
      botBluetooth = devices.get(item.getItemId()-listIdOffset);
      foundDevice=true;
      wl.acquire();
    } 
    return super.onOptionsItemSelected(item);
  }
}

void setup() {

  orientation(LANDSCAPE);
  devices = new ArrayList<BluetoothDevice>();
  listSize = 0;
  batteryVisible = true;
  isBoosted = false;

  //screen lock  
  PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
  wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "Communication with SmartBot");

  
  f = createFont("Arial", 30, true); // STEP 3 Create Font
  if (!bluetooth.isEnabled()) {
    //Intent requestBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
    //startActivityForResult(requestBluetooth, 0);
    bluetooth.enable();
    bluetoothEnabledAtStart = false;
  }
  /*If Bluetooth is now enabled, then register a broadcastReceiver to report any
   discovered Bluetooth devices, and then start discovering */
  if (bluetooth.isEnabled()) {
    registerReceiver(myDiscoverer, new IntentFilter(BluetoothDevice.ACTION_FOUND));
    //Start bluetooth discovery if it is not doing so already
  }


  screenWidth = getWindowManager().getDefaultDisplay().getWidth();
  screenHeight = getWindowManager().getDefaultDisplay().getHeight();
  /*
  camWidth = screenWidth = getWindowManager().getDefaultDisplay().getWidth();
  camHeight = screenHeight = getWindowManager().getDefaultDisplay().getHeight();

*/

  borderX = screenWidth - offset;
  borderY = screenHeight - offset;

  joyZero = new PVector(borderX + offset/2, borderY + offset/2);

  joyPos = new PVector(joyZero.x, joyZero.y);

  lightSource = new PVector(screenWidth/2, screenHeight/2, 0);
  /*
  try {
    if (background == null){
      background = loadImage("not_so_smart_bot.jpg");
    }
  }
  catch (OutOfMemoryError e) {
    print ("Unable to load background image");
  }
  */
  try {
    if (background == null)
      background = loadImage("/sdcard/stream.jpg");
  }
  catch (OutOfMemoryError e) {
    print ("Unable to load background image");
  }
  catch (Exception e2) {
    print ("Unable to load background image");
  }
  
  try {
    if (joystick == null)
      joystick = loadImage("joytsick-new-center.png");
  }
  catch (OutOfMemoryError e) {
    print ("Unable to load joystick image");
  }

  try {
    if (joystickBack == null)
      joystickBack = loadImage("joytsick-new.png");
  }
  catch (OutOfMemoryError e) {
    print ("Unable to load joystickBask image");
  }
  try {
    if (batteryImage == null)
      batteryImage = loadImage("bat.png");
  }
  catch (OutOfMemoryError e) {
    print ("Unable to load battery image");
  }
  try {
    if (nitroButtonOn == null)
      nitroButtonOn = loadImage("nitroOn.png");
  }
  catch (OutOfMemoryError e) {
    print ("Unable to load nitroOn image");
  }
  try {
    if (nitroButtonOff == null)
      nitroButtonOff = loadImage("nitroOff.png");
  }
  catch (OutOfMemoryError e) {
    print ("Unable to load accelerator image");
  }


  print(screenWidth);
  print(screenHeight);
}

void draw() {
  majBackground();
  if (background != null) {
    image(background, 0, 0, screenWidth, screenHeight);
  }
  else  {
    background(255, 255, 255);
  }

  textFont(f, 30);
  fill(255, 0, 0); 

  try {
    if (foundDevice) {
      print("in found device");
      UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"); //Standard SerialPortService ID
      mmSocket = botBluetooth.createRfcommSocketToServiceRecord(uuid);        
      mmSocket.connect();
      //print("connecté à linvor");
      OutputStream mmOutputStream = mmSocket.getOutputStream();
      InputStream mmInputStream = mmSocket.getInputStream();
      smartCom = new ComModule(mmOutputStream, mmInputStream, camWidth, camHeight);
      smartCom.start();
      foundDevice=false;
    }
  } 
  catch( Exception e ) {
    print("fault in draw, found device part");
    print(e);
  }

  updateJoystick();
  if (smartCom != null) {
    drawBattery(smartCom.getBateryLevel());
    //print("bat : " + smartCom.getBateryLevel());
  } else {
    drawBattery(100);
  }


  updateNitro();
}


void updateJoystick() {

  //joystick

    int borderX = screenWidth - offset;
  int borderY = screenHeight - offset;

  if (joystickBack != null) {
    image(joystickBack, joyZero.x - offset/2, joyZero.y - offset/2, offset, offset);
  } else {
    fill(200, 200, 0, 63);
    ellipse(joyZero.x, joyZero.y, offset, offset);
    fill(0);
  }


  if (isMousePressed) {
    PVector temp = new PVector(mouseX, mouseY);
    if (isMovedFromJoy || temp.dist(joyZero) < offset/2) {
      temp.sub(joyZero);
      float leng = temp.mag();
      temp.normalize();
      temp.mult(Math.min(offset/2, leng));
      joyPos.set(joyZero);
      joyPos.add(temp);
      isMovedFromJoy = true;
    }
  } else {
    joyPos.set(joyZero);
    isMovedFromJoy = false;
  }

  int Y = (int)(joyPos.y - joyZero.y);
  int X = (int)(joyPos.x - joyZero.x);

  if (smartCom != null) {
    smartCom.setX(X);
    smartCom.setY(Y);
    smartCom.setMaxSpeedLength(offset/2);
  }

  /*stroke(100, 100, 100, 100);
   strokeWeight(70);  // Beastly
   line(joyPos.x, joyPos.y, joyZero.x, joyZero.y);*/
  strokeWeight(0);
  stroke(0, 0, 0, 0);
  fill(0, 0, 0, shadowAlpha);

  PVector light = new PVector(joyPos.x - lightSource.x, joyPos.y - lightSource.y, 0);
  light.z = (float)Math.sqrt(light.mag())/lightRatio;
  //print(" x "+light.x+" y "+light.y+" z "+light.z);

  ellipse(joyPos.x + light.x/light.z, joyPos.y + light.y/light.z, joyWidth, joyWidth);

  if (joystick != null) {
    image(joystick, joyPos.x - joyWidth/2, joyPos.y - joyWidth/2, joyWidth, joyWidth);
  } else {
    fill(200, 200, 0, 126);
    ellipse(joyPos.x, joyPos.y, joyWidth, joyWidth);
  }
}



void mousePressed() {

  PVector temp = new PVector(mouseX, mouseY);

  if (mouseOnNitro()) {
    print("boooooooost");
    if (smartCom != null) {
      smartCom.turbo();
    }
    isBoosted = true;
    boostLaunchDate = millis();
  }
  isMousePressed = true;
}

void mouseReleased() {
  isMousePressed = false;
}

void mouseDragged() {

  PVector temp = new PVector(mouseX, mouseY);

  if (!isMovedFromJoy && temp.dist(joyZero) > offset/2) {

    lightSource.x = temp.x;
    lightSource.y = temp.y;
  }
}

boolean mouseOnNitro() {
  return (mouseX > nitroOffset && mouseX < nitroButtonWidth + nitroOffset
    && mouseY < screenHeight - nitroOffset && mouseY > screenHeight - nitroButtonWidth - nitroOffset
    && !isBoosted);
}
void stop() {
  if (!bluetoothEnabledAtStart) {
    bluetooth.disable();
  }
  print("stopping");
  super.stop();
}

/* This BroadcastReceiver will display discovered Bluetooth devices */
public class myOwnBroadcastReceiver extends BroadcastReceiver {
  @Override
    public void onReceive(Context context, Intent intent) {

    try {
      if (intent != null) {

        //Display the name of the discovered device
        String discoveredDeviceName = intent.getStringExtra(BluetoothDevice.EXTRA_NAME);
        ToastMaster("Discovered: " + discoveredDeviceName);
        print(discoveredDeviceName);


        //Display more information about the discovered device
        BluetoothDevice discoveredDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        if (discoveredDevice == null) {
          throw new NullPointerException();
        }
        devices.add(discoveredDevice);
        devicesList.add(1, listIdOffset + (listSize), Menu.NONE, discoveredDevice.getName());
        listSize += 1;
        ToastMaster("device found :" + discoveredDevice.getName());

        /*
   ToastMaster("getAddress() = " + discoveredDevice.getAddress());
         ToastMaster("getName() = " + discoveredDevice.getName());
         
         int bondyState=discoveredDevice.getBondState();
         ToastMaster("getBondState() = " + bondyState);
         
         String mybondState;
         switch(bondyState){
         case 10: mybondState="BOND_NONE";
         break;
         case 11: mybondState="BOND_BONDING";
         break;
         case 12: mybondState="BOND_BONDED";
         break;
         default: mybondState="INVALID BOND STATE";
         break;
         }
         ToastMaster("getBondState() = " + mybondState);
         */
        //Change foundDevice to true which will make the screen turn green
      }
    }
    catch (Exception e) {
    }
  }
}


/* My ToastMaster function to display a messageBox on the screen */
void ToastMaster(String textToDisplay) {
  Toast myMessage = Toast.makeText(getApplicationContext(), 
  textToDisplay, 
  0);
  myMessage.setGravity(Gravity.CENTER, 0, 0);
  myMessage.show();
}


void drawBattery(int percentage) {
  int batteryWidth = 250;
  int batteryHeight = 50;
  int realBatteryWidth = 250 + 12;
  int realBatteryHeight = 50 + 10;
  int batteryX = screenWidth/2 - batteryWidth/2;
  int batteryY = 25;

  PVector batteryPos = new PVector(batteryX - 6 + realBatteryWidth/2, batteryY - 5 + realBatteryHeight/2);
  PVector light = new PVector(batteryPos.x - lightSource.x, batteryPos.y - lightSource.y, 0);
  light.z = (float)Math.sqrt(light.mag())/lightRatio*2;

  if (batteryVisible) {

    fill(0, 0, 0, shadowAlpha);
    rect(batteryPos.x - realBatteryWidth/2 + light.x/light.z, batteryPos.y - realBatteryHeight/2 + light.y/light.z, realBatteryWidth, realBatteryHeight);

    fill(0, 255, 0, 255);
    rect(batteryX, batteryY, (percentage/100.0)*batteryWidth, batteryHeight);
    if (batteryImage != null) {
      image(batteryImage, batteryPos.x - realBatteryWidth/2, batteryPos.y - realBatteryHeight/2, realBatteryWidth, realBatteryHeight);
    }
  }
}


void updateNitro() {

  PVector nitroPos = new PVector(nitroOffset + nitroButtonWidth/2, screenHeight - nitroButtonWidth - nitroOffset + nitroButtonWidth/2);
  PVector light = new PVector(nitroPos.x - lightSource.x, nitroPos.y - lightSource.y, 0);

  fill(0, 0, 0, shadowAlpha);

  if (isBoosted && millis() - boostLaunchDate < maxNitroTimer) {
    light.z = ((float)Math.sqrt(light.mag())/lightRatio)*2;
    ellipse(nitroPos.x + light.x/light.z, nitroPos.y + light.y/light.z, nitroButtonWidth, nitroButtonWidth);
    image(nitroButtonOff, nitroPos.x - nitroButtonWidth/2, nitroPos.y - nitroButtonWidth/2, nitroButtonWidth, nitroButtonWidth);
  } else {
    isBoosted = false;
    light.z = ((float)Math.sqrt(light.mag())/lightRatio);
    ellipse(nitroPos.x + light.x/light.z, nitroPos.y + light.y/light.z, nitroButtonWidth, nitroButtonWidth);
    image(nitroButtonOn, nitroPos.x - nitroButtonWidth/2, nitroPos.y - nitroButtonWidth/2, nitroButtonWidth, nitroButtonWidth);
  }
}

void majBackground() {
  //print("in maj back");
  try {
    background = loadImage("/sdcard/stream.jpg");
  }
  catch (OutOfMemoryError e) {
    //print ("Unable to load background image");
  }
  catch (Exception e2) {
    //print ("Unable to load background image");
  }
}


void showTrimerDialog() {
    //Création de l'AlertDialog
  AlertDialog.Builder adb = new AlertDialog.Builder(this);

  //On donne un titre à l'AlertDialog
  adb.setTitle("Trimer");

  LinearLayout group = new LinearLayout(this);
  group.setOrientation(LinearLayout.HORIZONTAL);

  Button trimLeft = new Button(this);
  trimLeft.setText("trim left");
  trimLeft.setHeight(100);
  trimLeft.setWidth(200);

  trimLeft.setOnClickListener(new View.OnClickListener() {
    public void onClick(View v) {

      print("triming left");
      if (smartCom != null) {
        smartCom.trimLeft();
      }
    }
  }
  );


  Button trimRight = new Button(this);
  trimRight.setText("triming right");
  trimRight.setHeight(100);
  trimRight.setWidth(200);

  trimRight.setOnClickListener(new View.OnClickListener() {
    public void onClick(View v) {

      print("triming right");
      if (smartCom != null) {
        smartCom.trimRight();
      }
    }
  }
  );


  group.addView(trimLeft);
  group.addView(trimRight);

  adb.setView(group);

  //On modifie l'icône de l'AlertDialog pour le fun ;)
  //adb.setIcon(android.R.drawable.ic_dialog_alert);

  //On affecte un bouton "OK" à notre AlertDialog et on lui affecte un évènement
  adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
    public void onClick(DialogInterface dialog, int which) {

      if (smartCom != null) {
        smartCom.trimOk();
      }
      //On affiche dans un Toast le texte contenu dans l'EditText de notre AlertDialog
    }
  }
  );
  adb.show();
}
