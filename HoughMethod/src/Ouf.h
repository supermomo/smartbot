#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/core/core_c.h>

#include <cctype>
#include <iostream>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <fcntl.h>
#include <unistd.h>

#include "MSAC2.h"


class Ouf
{
public:
	Ouf(int usbDesc);
	int houghSmartUpdate ();
private:

    std::string imgExt;

    std::string imgPath;

	cv::Point direction;
	cv::Point delta;
	cv::Point meanVanish;
	int cameraNumber;
	CvCapture* capture;
	int usbFileDesc;
	double maxMagn;
	double lastTime;
	int screenWidth;
	int screenHeight;
	int borderDelta;
	MSAC msac;
	unsigned int lastVpsSize;
    std::vector<cv::Point> lastVPs;

	//Hough variables
	double minIntersection;
	double thetaMin;
	double rhoMin;


	void move(cv::Point target);
	bool isInBound(const int& offset, const int& minX, const int& minY, const int& maxX,
	const int& maxY, const int& valX, const int& valY);
	
};
