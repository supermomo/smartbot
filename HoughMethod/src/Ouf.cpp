#include "Ouf.h"

using namespace cv;
using namespace std;


Ouf::Ouf(int usbDesc) 
    : cameraNumber(0),
    usbFileDesc(usbDesc),
    msac(),
    lastVpsSize(20)
{
    cout<<"initialisation\n";

    imgExt = ".jpg";
    imgPath = "";
    //imgPath = "/home/pi/Detector/screen/";

    Mat frame = Mat::zeros(200, 320, CV_8UC3);
    capture = cvCaptureFromCAM(cameraNumber);

    if(!capture) {
        cout << "Capture from CAM didn't work" << endl;
    } else {

        frame = cvQueryFrame( capture );

        screenWidth = frame.cols;
        screenHeight = frame.rows;
        borderDelta = frame.cols/8;
        delta.x = frame.cols/10;
        delta.y = frame.rows/10;
        direction.x = screenWidth/2;
        direction.y = screenHeight*3.0/4.0;
        maxMagn = screenHeight*2.0/4.0;

        msac.init(MODE_NIETO, cvSize(screenWidth, screenHeight));

        //Hough variables
        minIntersection = frame.rows / 10; // decrease divider to have less lines
        thetaMin = 1;//do not change
        rhoMin = 8.0*CV_PI/180.0;//15 degree : increase to have less lines
        lastTime = 0;
    }
}

int Ouf::houghSmartUpdate(){

    Mat frameCopy;

    Mat frame = Mat::zeros(200, 320, CV_8UC3);
    Mat dst, cdst;

    cv::Point target;
   
    frame = cvQueryFrame( capture );

    if( frame.empty() ){
        return -1;
    }

    frame.copyTo( frameCopy );            
    
    /// Generate grad_x and grad_y
    Mat grad_x, grad_y;
    Mat abs_grad_x, abs_grad_y;
    cvtColor(frameCopy, frameCopy, CV_BGR2HSV);


    int scale = 3;
    int deltaa = 0;
    int ddepth = CV_16S;

    std::vector<cv::Mat> hsvChannels(3);
    cv::split(frameCopy, hsvChannels);

    Mat sobel = hsvChannels[2];
    
    /// Gradient X
    Sobel( sobel, grad_x, ddepth, 1, 0, 3, scale, deltaa, BORDER_DEFAULT );
    convertScaleAbs( grad_x, abs_grad_x );

    /// Gradient Y
    Sobel( sobel, grad_y, ddepth, 0, 1, 3, scale, deltaa, BORDER_DEFAULT );
    convertScaleAbs( grad_y, abs_grad_y );

    /// Total Gradient (approximate)
    addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, dst );

    /* 
    0: Binary
    1: Binary Inverted
    2: Threshold Truncated
    3: Threshold to Zero
    4: Threshold to Zero Inverted
    */
    threshold( dst, dst, 180, 255, 0 );

    dst.copyTo(cdst);

    vector<cv::Vec4i> lines;
    HoughLinesP(cdst, lines, thetaMin, rhoMin, minIntersection, minIntersection/10, 20);

    Mat lineView;
    frame.copyTo(lineView);
    
    vector< vector<cv::Point> > linesPoints;

    for(size_t i = 0; i < lines.size(); ++i){

        Point pt1(lines[i][0], lines[i][1]);
        Point pt2(lines[i][2], lines[i][3]);

        cv::clipLine(frameCopy.size(), pt1, pt2);

        line(lineView, pt1, pt2, Scalar(0,0,255), 1, 8);

        vector <cv::Point> tempLine;
        tempLine.push_back(pt1);
        tempLine.push_back(pt2);
        linesPoints.push_back(tempLine);

    }

    

    std::vector<cv::Mat> vps;
    std::vector<int> numInliers;
    std::vector<std::vector<std::vector<cv::Point> > > lineSegmentsClusters;

    if( !linesPoints.empty()){
        
        msac.multipleVPEstimation(linesPoints, lineSegmentsClusters, numInliers, vps, 4);
        
        int meanX = 0;
        int meanY = 0;

        for(unsigned int i = 0; i<vps.size(); ++i){

            Mat matRes(vps[i]);
            float v1 = matRes.at<float>(0,0);
            float v2 = matRes.at<float>(1,0);
            
            Point vanish(v1, v2);

            if(isInBound(borderDelta ,0, 0, screenWidth, screenHeight, vanish.x, vanish.y)){

                // cout<< "isInBound\n";
                if(lastVPs.size() >= lastVpsSize){
                    lastVPs.erase(lastVPs.begin());
                }
                lastVPs.push_back(vanish);

                vector<cv::Point>::iterator it;

                meanX = 0;
                meanY = 0;

                for(it = lastVPs.begin(); it != lastVPs.end(); ++it){
                    meanX += it->x;
                    meanY += it->y;
                }

                meanX /= lastVPs.size();
                meanY /= lastVPs.size();

                meanVanish.x = meanX;
                meanVanish.y = meanY;

                cv::circle(lineView, vanish, 10, Scalar(255,0,0), -1);  
                 
            } else {
	      //cout<<"vanishing point found but not in bound\n";
            }

        }

        move(meanVanish);

    } else {
      //cout<<"no vanishing point, wait for instruction/unstucking\n";
    }

    cv::circle(lineView, meanVanish, 10, Scalar(0,255,255), -1);  
    cv::circle(lineView, direction, 10, Scalar(0,255,0), -1); 
    //imshow("lines", lineView);
    //imshow("Capture", frameCopy);
    //imshow("Canny", dst);

    //every five seconds
    if((double)(cvGetTickCount()/cvGetTickFrequency())/1000000.0 - lastTime > 5.0 ){

        lastTime = (double)(cvGetTickCount()/cvGetTickFrequency())/1000000.0;
        std::stringstream ss;
        ss << imgPath;
        ss << (int)lastTime;
        ss << imgExt;
        //write the image
	cout << ss.str() << endl;
        imwrite(ss.str(), frame);
	cout<<"write-->>\n";
    }

    char key = waitKey(20);
    if (key == 'q') {
        exit(0);
    }

    return (int) lastTime;
}


void Ouf::move(cv::Point target){

    Point motion (target.x - direction.x, target.y - direction.y);

    //angle computation
    double angle = atan2(motion.y, motion.x);
    angle = angle * (-1/(CV_PI)) * 8;
    Point vec(direction.x + motion.x, direction.y + motion.y);

    int magn = sqrt(motion.x * motion.x + motion.y * motion.y);
    //normalize magnitude
    magn = magn/maxMagn * 7;

    if(angle < 0){
        angle = angle * -1;
        magn = magn * -1;
    }
    int speed = magn;
    //cout<<"angle " << angle << endl;
    //cout<<"speed " << magn << endl;

    if(speed > 7){
        speed = 7;
    } else if( speed < -7){
        speed = -7;
    }

    int angleInt = cvRound(angle);
    char res = speed + angleInt * 16;
    //cout<<"sending : "<<res<<endl;

    char codeForSmartCmd = 0xFF;
    write(usbFileDesc, &codeForSmartCmd, sizeof(char));
    codeForSmartCmd = 0xD7;
    write(usbFileDesc, &codeForSmartCmd, sizeof(char));
    write(usbFileDesc, &res, sizeof(char));
}

bool Ouf::isInBound(const int& offset, const int& minX, 
    const int& minY, const int& maxX, const int& maxY,
     const int& valX, const int& valY){
    return valX > minX + offset && valX < maxX - offset && valY > minY + offset && valY < maxY - offset;
}



/*int main (int argc, char** args) {

    Ouf ouf(0);
    cout<<"done\n";
    for(;;){
        ouf.houghSmartUpdate();
        cout<<"oufing\n";
    }
}*/
