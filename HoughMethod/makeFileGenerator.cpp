
#include <vector>
#include <string>
#include <cstdio>
#include <iostream>
using namespace std;

/*
* Return the target it it exist
* return NULL otherwise
*/
const char* containTarget(char *str){
    string s(str);
    unsigned int pos=s.find(":");
    if(pos<s.length()){
        return s.substr(0,pos).c_str();
    }
    return NULL;
}

/*
* Return the name (with the extension attached) of the cpp file in the string 
*/
string extractCPP(const char * str){
    string s(str);
    unsigned int pos=s.find(":");
    unsigned int end=s.find(".cpp");
    return s.substr(pos+1,end+4-pos);
}

/*
* Return the string freed from the backslash that might have been at the end of it
* If there is no backslash, the string is return unmodified
*/
string removeBackslash(char * str){
    string s(str);
    unsigned int pos=s.find("\\");
    if(pos<s.length()){
        return s.substr(0,pos);
    }
    return s;
}

int main(int argc, char** args){
    
    if(argc > 1) {

        string searchCmd("");
        searchCmd+="g++ -MM ";
        searchCmd+=args[1];
        //searchCmd+=".cpp";

        vector<string> libLinking;

        if(argc>2){
            for(int i=2;i<argc;++i){
                libLinking.push_back(*new string(args[i]));
            }   
        }

        const char * cmds=searchCmd.c_str();

        FILE* file=popen(cmds, "r");
        FILE* makefile=fopen("makefile", "w+");
        FILE* temp=fopen("temp", "w+");
        vector<string*> targets;
        vector<string*>::iterator it;
        char actualLine[164];
        const char* target;
        string compilCPP;
        string* sTarget;
        string objectToBuild;

        fputs("ALL = Run.exe\n\n\0",makefile);
        fputs("BUILDDIR = Build/\n\n\0",makefile);
        fputs("# Targets\n\n\0",makefile);

        //Write the rules returned by the command in a temporary file
        while(fgets(actualLine,164,file)){

            target=containTarget(actualLine);
            if(target!=NULL){
                sTarget=new string(target);
                //We are not interested in the makefileGenerator rule so we don't add it to our targets vector
                if(sTarget->find("makeFileGenerator") > sTarget->length()){
                    //write compilCPP
                    if(!compilCPP.empty()){
                        fputs(compilCPP.c_str(), temp);
                        //Add the part creatiing the object files into the build directory
                        objectToBuild = " -o $(BUILDDIR)";
                        objectToBuild +=*targets[targets.size()-1] +"\n";
                        fputs(objectToBuild.c_str(), temp);
                    }
                    targets.push_back(new string(target));
                    compilCPP= "\n\t-g++ -g -Wall -c "+extractCPP(actualLine);
                    fputs("\n\n\0",temp);
                }
                delete sTarget;
            }
            //We don't want to write anything about the makeFilegenerator
            if(target==NULL || (new string(target))->find("makeFileGenerator")==string::npos){
                fputs(removeBackslash(actualLine).c_str(),temp);
            }        
            
        }
        if(!compilCPP.empty()){
            fputs(compilCPP.c_str(), temp);
            //Add the part creatiing the object files into the build directory
            objectToBuild = " -o $(BUILDDIR)";
            objectToBuild +=*targets[targets.size()-1] +"\n";
            fputs(objectToBuild.c_str(), temp);
        }

        //Write the Run.exe rule a the top of the file
        fputs("\n\nRun.exe : ", makefile);
        for(it=targets.begin();it!=targets.end();++it){
            fputs((*it)->c_str(),makefile);
            fputs(" ",makefile);
        }
        //Write the compilation command right below it
        fputs("\n\t-g++ -g -Wall", makefile);

        fputs(" -o ", makefile);
        fputs("$(BUILDDIR)", makefile);
        fputs("Run.exe ", makefile);
        for(it=targets.begin();it!=targets.end();++it){
            //Add the build directory to the object files path
            (*it)->insert(0, "$(BUILDDIR)");
            fputs((*it)->c_str(),makefile);
            fputs(" ",makefile);
        }

        if(argc>2){
            for(unsigned int i=0;i<libLinking.size();++i){
                fputs(" -l", makefile);
                fputs(libLinking[i].c_str(), makefile);
            }
        }
        
        //Addional commands
        fputs("\n\t-[ -e \"Build\" ] || mkdir \"Build\"", makefile);
        //fputs("\n\t-mv *.o *.exe Build", makefile);

        fclose(temp);
        temp=fopen("temp","r");

        //Write the rest of the rules (from the temp file) into the makefile
        while(fgets(actualLine,164,temp)){
            fputs(actualLine,makefile);
        }
        //Add the clean rule
        fputs("\n\nclean : \n\t-rm *.o *.exe */*.o */*.exe\0", makefile);

        //Add an aditional rule for easy making
        fputs("\n\nmake : \n\t-./a.out 'src/*.cpp' Irrlicht GL GLU Xrandr Xext X11\0", makefile);

        fclose(temp);
        remove("temp");
        fclose(makefile);
        pclose(file);
    }
}