#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/core/core_c.h>

#include <cctype>
#include <iostream>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <fcntl.h>
#include <unistd.h>

#include "MSAC.h"

using namespace std;
using namespace cv;


int main () {

    CvCapture* capture = 0;
    Mat frameCopy;

    Mat frame = Mat::zeros(200, 320, CV_8UC3);

    capture = cvCaptureFromCAM(1);

    if(!capture) 
        cout << "Capture from CAM didn't work" << endl;
    else
    {

        cout << "In capture ..." << endl;
        for(;;)
        {
            IplImage* iplImg = cvQueryFrame( capture );
            frame = iplImg;
            if( frame.empty() )
                break;

            MSAC msac = *new MSAC();
            msac.init(frame.cols, frame.rows, 1);

            frame.copyTo( frameCopy );

            //stuff
            //

            Mat dst, cdst;
            Canny(frameCopy, dst, 50, 200, 3);
            dst.copyTo(cdst);
            //cvtColor(dst, cdst, CV_GRAY2BGR);

            vector<cv::Vec2f> lines;
            int threshold = frameCopy.rows / 8; // a peu prés ^^
            HoughLines(cdst, lines, 1, CV_PI/180, threshold);
            
            Mat lineView;
            frameCopy.copyTo(lineView);
            //cvtColor(cdst, lineView, CV_GRAY2BGR);
            vector< vector<CvPoint> > linesPoints;
            for(size_t i = 0; i < lines.size(); ++i){

                float rho = lines[i][0];
                float theta = lines[i][1];

                double a = cos(theta);
                double b = sin(theta);
                double x0 = a * rho;
                double y0 = b * rho;

                Point pt1( cvRound(x0 + 1000 * (-b)),
                    cvRound(y0 + 1000 * (a)));
                Point pt2( cvRound(x0 - 1000 * (-b)),
                    cvRound(y0 - 1000 * (a)));

                cv::clipLine(frameCopy.size(), pt1, pt2);

                line(lineView, pt1, pt2, Scalar(0,0,255), 1, 8);

                CvPoint cp1 = cvPoint(pt1.x, pt1.y);
                CvPoint cp2 = cvPoint(pt2.x, pt2.y);
                vector <CvPoint> tempLine;
                tempLine.push_back(cp1);
                tempLine.push_back(cp2);
                linesPoints.push_back(tempLine);

            }

            CvMat* resVp = cvCreateMat(3, 1, CV_32FC1);
            int n_of_inliers = 0;
            cout<<"here "<<linesPoints[0].size()<< endl;
            msac.singleVPEstimation(linesPoints, &n_of_inliers, resVp);
            Mat matRes(resVp);
            float v1 = matRes.at<float>(0,0);
            float v2 = matRes.at<float>(1,0);
            float v3 = matRes.at<float>(2,0);
            cout<<v1<<" "<< v2 <<" "<< v3 <<endl;
            Point vanish(v1, v2);
            cv::circle(lineView, vanish, 10, Scalar(255,0,0));
            imshow("lines", lineView);
            imshow("Canny", dst);
            imshow("Capture", frameCopy);
            char key = waitKey(20);
            if (key == 'q')
                exit(0);
        }

    }

}
