#pragma once

#include <opencv2/core/core_c.h>
#include <opencv2/highgui/highgui.hpp>
#include <stdio.h>
#include <stdlib.h>

#include <math.h>
//#define PI 3.1415926535
#ifndef ABS
#define ABS(a)	   (((a) < 0) ? -(a) : (a))
#endif

//#define DEBUG_VERBOSE

class MSAC
{
public:
	// RANSAC
	// Image info
	int width;
	int height;

	// Options 
	float epsilon;
	float P_inlier;
	float T_noise_squared;
	int min_iters;
	int max_iters;
	bool reestimate;
	bool verbose;
	bool update_T_iter;
	bool notify;

	// Parameters (precalculated)
	int minimal_sample_set_dimension;
	int N_I_best;
	float J_best;
	CvMat* v_best;
	CvMat* v_est;
	std::vector<int> MSS;

	int number_of_vps;

	// Normalized Homogeneous MLE
	CvMat* Q, *Q_inv;
	CvMat* LSS;

	// Consensus set
	CvMat* CS_idx;
	CvMat* CS_best;
	int* ind_CS_best;
	double vp_length_ratio;
	
	// Tracking
	bool track;
	CvMat* v_est_prev;


	MSAC(void);
	~MSAC(void);

	void setTrack(bool);
	void init(int, int, int);
	void msac(std::vector<std::vector<CvPoint> > &, std::vector<int> &, int *, CvMat *, int, bool);	
	void singleVPEstimation(std::vector<std::vector<CvPoint> > &lines, int *number_of_inliers, CvMat *vanishing_point);
	void GetMinimalSampleSet(CvMat *, CvMat *, std::vector<int> &, CvMat *);
	void EstimateVanishingPoint(CvMat *, CvMat *, std::vector<int> &, int, CvMat *);
	float GetConsensusSet(CvMat *, CvMat *, float *, int *);
	void trackingMSAC(float *, std::vector<std::vector<CvPoint> > &, int);

	int getNumberVPS();
};
