#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/core/core_c.h>

#include <cctype>
#include <iostream>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <fcntl.h>
#include <unistd.h>

using namespace std;
using namespace cv;

void negative(const cv::Mat src, cv::Mat& dest);

void rescaleRect(cv::Rect& rect, cv::Rect& ref);

void writeToFile(const int& file, IplImage* iplImg, int width, int height);

void ObstacleDetect(int* fd);

const int camNum = 1;

const int horizontalLimit = 50;
const int sideWidth = 25;


void negative(const Mat src, Mat& dest) {
  
  Mat new_image = Mat::zeros( src.size(), src.type() );
 
  // create a matrix with all elements equal to 255 for subtraction
  Mat sub_mat = Mat::ones(src.size(), src.type())*255;
 
  //subtract the original matrix by sub_mat to give the negative output new_image
  subtract(sub_mat, src, new_image);

  new_image.copyTo(dest);
}


void rescaleRect(Rect& rect, Rect& ref) {  
  if (rect.x < ref.x) {
    rect.x = ref.x;
  }
  if (rect.x > ref.x + ref.width) {
    rect.x = ref.x + ref.width;
  }
  if (rect.y < ref.y) {
    rect.y = ref.y;
  }
  if (rect.y > ref.y + ref.height) {
    rect.y = ref.y + ref.height;  
  }
  if (rect.x + rect.width > ref.x + ref.width) {
    rect.width = ref.x + ref.width - rect.x;
  } 
  if (rect.y + rect.height > ref.y + ref.height) {
    rect.height = ref.y + ref.height - rect.y;
  }  
}


void writeToFile(const int& file, IplImage* iplImg, int width, int height){


	for(int x = 0; x < width; ++x){

		for(int y = 0; y < height; ++y){

			CvScalar pixel;
			pixel = cvGet2D(iplImg, y, x); // get the (i,j) pixel value

			char b = pixel.val[0]; //is the blue component value
			char g = pixel.val[1]; //is the green component value
			char r = pixel.val[2]; //is the red component value
			write(file,&b,sizeof(char));
			write(file,&g,sizeof(char));
			write(file,&r,sizeof(char));

		}

	}

	//write(file,iplImg->imageData,sizeof(char)*(iplImg->imageSize));

	char c = '\u000A';
	write(file,&c,sizeof(char));

}



void ObstacleDetect(int* fd) {

  int histogramme = 0;

  int vmin = 10, vmax = 230, smin = 40;

  CvCapture* capture = 0;
  Mat frameCopy;

  Mat frame, hsv, hue, mask, hist = Mat::zeros(200, 320, CV_8UC1), backproj;
  Mat histImg, negBackproj;
  bool backprojMode = false;

  Rect bottom, top, left, right;
  Rect trackBottom, trackTop, trackLeft, trackRight;

  int hbins = 32, sbins = 32;
  int hsize_hist[] = {hbins, sbins};
  float hranges[] = {0,180};
  float sranges[] = {0,256};
  const float* phranges[] = {hranges, sranges};
  int channels[] = {0, 1};

  string inputName;

  capture = cvCaptureFromCAM(camNum);

  if(!capture) 
    cout << "Capture from CAM didn't work" << endl;
  else
    {
      cout << "In capture ..." << endl;
      for(;;)
        {
	  IplImage* iplImg = cvQueryFrame( capture );
	  frame = iplImg;
	  if( frame.empty() )
	    break;
	  frame.copyTo( frameCopy );
	  // resize(frameCopy,frameCopy,cvSize(32,24));

	  // writeToFile(*fd, iplImg, 32, 24);

	  // cout << "Writing img\n" << endl;
	  /*write(*fd,iplImg->imageData,sizeof(char)*(iplImg->imageSize));
	  char c = '\u000A';
	  write(fd,&c,sizeof(char));*/
	  cvtColor(frameCopy, hsv, CV_BGR2HSV);

	  int bottomWidth = frameCopy.cols - ((2*sideWidth)/100.0)*(frameCopy.cols);
	  int bottomHeight = frameCopy.rows - (horizontalLimit/100.0)*frameCopy.rows;
	  int bottomX = (sideWidth/100.0)*frameCopy.cols;
	  int bottomY = (horizontalLimit/100.0)*frameCopy.rows;
	  bottom = Rect(bottomX, 
			   bottomY, 
			   bottomWidth, 
			   bottomHeight);

	  int topWidth = bottomWidth;
	  int topHeight = (horizontalLimit/100.0)*frameCopy.rows;
	  int topX = bottomX;
	  int topY = 0;
	  top = Rect(topX, 
		     topY, 
		     topWidth, 
		     topHeight);

	  int leftWidth = (sideWidth/100.0)*frameCopy.cols;
	  int leftHeight = frameCopy.rows;
	  int leftX = 0;
	  int leftY = 0;
	  left = Rect(leftX, 
		     leftY, 
		     leftWidth, 
		     leftHeight);

	  int rightWidth = leftWidth;
	  int rightHeight = leftHeight;
	  int rightX = frameCopy.cols - rightWidth;
	  int rightY = 0;
	  right = Rect(rightX, 
		     rightY, 
		     rightWidth, 
		     rightHeight);


	  inRange(hsv, Scalar(0, smin, MIN(vmin,vmax)),
		  Scalar(180, 256, MAX(vmin, vmax)), mask);

	  cout << "hist.type() :" << hist.type() << endl;
	  Mat roi(hsv, bottom), maskroi(mask, bottom);

	  if(!histogramme) {
	    imshow("roi", roi);
	    imshow("maskroi", maskroi);
	    calcHist(&roi, 1, channels, maskroi,
		   hist, 2, hsize_hist, phranges,
		   true,
		   false);
	    histogramme = 42;
	  }

	  
	  cout << "CV_8UC1 :" << CV_8UC1 << endl; 
	  cout << "hist.type() :" << hist.type() << endl;
	  normalize(hist, hist, 0, 255, CV_MINMAX);

	  
	  trackBottom = bottom;
	  trackTop = top;
	  trackLeft = left;
	  trackRight = right;

	  double maxVal=0;
	  minMaxLoc(hist, 0, &maxVal, 0, 0);
	  int scale = 10;
	  histImg = Mat::zeros(sbins*scale, hbins*10, CV_8UC3);


	  for( int h = 0; h < hbins; h++ )
	    for( int s = 0; s < sbins; s++ )
	      {
		float binVal = hist.at<float>(h, s);
		int intensity = cvRound(binVal*255/maxVal);
		rectangle( histImg, Point(h*scale, s*scale),
			   Point( (h+1)*scale - 1, (s+1)*scale - 1),
			   Scalar::all(intensity),
			   CV_FILLED );
	      }

	  vector<cv::Mat> histchannels(2);
	  cv::split(hist, histchannels);
	  Mat hist2 = Mat::zeros(hist.cols, hist.rows, CV_8UC1);
	  cout << "hist2.type() :" << hist2.type() << endl;
	  histchannels[0].convertTo(hist2, CV_8U);
	  cout << "hist2.type() :" << hist2.type() << endl;

	  equalizeHist(hist2, hist);

	  calcBackProject(&hsv, 1, channels, hist, backproj, phranges);
	  negative(backproj, negBackproj);
	  
	  backproj &= mask;
	  Rect boxTop, boxBottom, boxLeft, boxRight;
	  boxBottom = CamShift(negBackproj, trackBottom,
			       TermCriteria( CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 10, 1 )).boundingRect();

	  boxTop = CamShift(negBackproj, trackTop,
			    TermCriteria( CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 10, 1 )).boundingRect();

	  boxLeft = CamShift(negBackproj, trackLeft,
			    TermCriteria( CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 10, 1 )).boundingRect();

	  boxRight = CamShift(negBackproj, trackRight,
			    TermCriteria( CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 10, 1 )).boundingRect();


	  // Mat topCopy, bottomCopy, leftCopy, rightCopy;
	  rescaleRect(boxTop, top);
	  // topCopy = frameCopy(boxTop);
	  
	  rescaleRect(boxBottom, bottom);
	  // bottomCopy = frameCopy(boxBottom);	  
	  
	  rescaleRect(boxLeft, left);
	  // leftCopy = frameCopy(boxLeft);

	  rescaleRect(boxRight, right);
	  // rightCopy = frameCopy(boxRight);
	  
	  if (boxTop.area()/((float)top.area()) > 60/100.0) {
	    // Obstacle on the top
	    cout << "obstacle on the top !" << endl;
	  }

	  if (boxBottom.area()/((float)bottom.area()) > 15/100.0) {
	    // Obstacle on the bottom
	    cout << "obstacle on the bottom !" << endl;
	  }

	  if (boxLeft.area()/((float)left.area()) > 70/100.0) {
	    // Obstacle on the left
	    cout << "obstacle on the left !" << endl;
	  }

	  if (boxRight.area()/((float)right.area()) > 70/100.0) {
	    // Obstacle on the right
	    cout << "obstacle on the right !" << endl;
	  }
	  
	  rectangle(frameCopy, boxBottom, Scalar(0,0,255));
	  rectangle(frameCopy, boxTop, Scalar(0,255,0));
	  rectangle(frameCopy, boxRight, Scalar(255,0,0));
	  rectangle(frameCopy, boxLeft, Scalar(255,255,255));
	  imshow("Capture", frameCopy);
	  imshow("Backproj", backproj);
	  imshow("Negative backproj", negBackproj);
	  char key = waitKey(20);
	  if (key == 'q') {
	    cout << "Exiting ..." << endl;
	    exit(0);
	  }
	}

    }

}


int main () {
  int fd;
  struct termios tty, old;
  /* Ouverture du device */
  // if ((fd = open ("/dev/ttyAMA0", O_RDWR | O_NDELAY)) < 0) 
  // {
  //   perror ("/dev/ttyAMA0");
  //   exit (1);
  // }
  // tcgetattr(fd, &tty);

  // cfsetospeed(&tty, B115200);
  // cfsetispeed(&tty, B115200);
  
  // tcsetattr(fd, TCSANOW, &tty);


  ObstacleDetect(&fd);

  close(fd);

  return 0;
}
