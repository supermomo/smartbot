#include "MSAC.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>

MSAC::MSAC(void)
{
}

MSAC::~MSAC(void)
{
	cvReleaseMat(&v_best);
	cvReleaseMat(&v_est);
	cvReleaseMat(&Q);
	cvReleaseMat(&Q_inv);
	cvReleaseMat(&v_est_prev);

}
void MSAC::init(int imWidth, int imHeight, int number_of_vps)
{
	// Options
	epsilon = (float)1e-6;
	P_inlier = (float)0.95;	
	T_noise_squared = (float)0.01623*2;
	min_iters = 5;
	max_iters = INT_MAX;
#ifdef DEBUG_VERBOSE
	verbose = true;
#else
	verbose = false;
#endif
	update_T_iter = false;

	// Parameters
	minimal_sample_set_dimension = 2;
	v_best = cvCreateMat(3,1, CV_32F);
	v_est = cvCreateMat(3,1,CV_32F);
	
	// Minimal Sample Set
	for(int i=0; i<minimal_sample_set_dimension; ++i) MSS.push_back(0);
	
	// N-Hom. MLE
	Q = cvCreateMat(3,3,CV_32F);
	Q_inv = cvCreateMat(3,3,CV_32F);
	float f = (float)imWidth;
	Q->data.fl[0] = 1/f;
	Q->data.fl[1] = 0;
	Q->data.fl[2] = -imWidth/(2*f);

	Q->data.fl[3] = 0;
	Q->data.fl[4] = 1/f;
	Q->data.fl[5] = -imHeight/(2*f);

	Q->data.fl[6] = 0;
	Q->data.fl[7] = 0;
	Q->data.fl[8] = 1;

	Q_inv->data.fl[0] = f;
	Q_inv->data.fl[1] = 0;
	Q_inv->data.fl[2] = (float)imWidth/2;

	Q_inv->data.fl[3] = 0;
	Q_inv->data.fl[4] = f;
	Q_inv->data.fl[5] = (float)imHeight/2;

	Q_inv->data.fl[6] = 0;
	Q_inv->data.fl[7] = 0;
	Q_inv->data.fl[8] = 1;

	this->width = imWidth;
	this->height = imHeight;

	this->number_of_vps = number_of_vps;
	v_est_prev = cvCreateMat(3,number_of_vps,CV_32F);	
	track = false;

}

void MSAC::msac(std::vector<std::vector<CvPoint> > &lines, std::vector<int> &ind_CS_best, int* number_of_inliers, CvMat* vanishing_point, int num_vp, bool vp_prev_exists)
{	
	int num_lines = lines.size();

	if (v_est->rows == 1)
		cvTranspose(v_est,v_est);	

	// Return 0 if the number of elements is lower than minimal sample set
	if (num_lines < minimal_sample_set_dimension)
	{
#ifdef DEBUG_VERBOSE
		printf("Number of line segments must be higher than minimal sample set");
#endif
		*number_of_inliers = 0;		
	}	
	else
	{
		// Main loop
		N_I_best = minimal_sample_set_dimension;
		J_best = FLT_MAX;

		CS_best = cvCreateMat(1, num_lines, CV_32F);
		CS_idx = cvCreateMat(1, num_lines, CV_32F);
		cvZero(CS_best);
		cvZero(CS_idx);
		
		int iter = 0;
		int T_iter = INT_MAX;
		int no_updates = 0;
		int max_no_updates = INT_MAX;

		// Allocate Error matrix
		float* E = (float*) malloc(sizeof(float)*num_lines);

		// Transform all line segments
		LSS = cvCreateMat(num_lines, 3, CV_32F);
		CvMat* a = cvCreateMat(3,1,CV_32F);
		CvMat* b = cvCreateMat(3,1,CV_32F);
		CvMat* an = cvCloneMat(a);
		CvMat* bn = cvCloneMat(b);
		CvMat* li = cvCreateMat(3,1,CV_32F);
		
		// Loop over line segments		
		CvMat* Lengths = cvCreateMat(num_lines, num_lines, CV_32F);
		cvSetZero(Lengths);
		double sum_lengths = 0;
		for (int i=0; i<num_lines; i++)
		{
			// Extract the end-points					
			CvPoint p1 = lines[i][0];
			CvPoint p2 = lines[i][1];
			a->data.fl[0] = (float)p1.x;
			a->data.fl[1] = (float)p1.y;
			a->data.fl[2] = 1;
			b->data.fl[0] = (float)p2.x;
			b->data.fl[1] = (float)p2.y;
			b->data.fl[2] = 1;	

			double length = sqrt((b->data.fl[0]-a->data.fl[0])*(b->data.fl[0]-a->data.fl[0]) + (b->data.fl[1]-a->data.fl[1])*(b->data.fl[1]-a->data.fl[1]));
			sum_lengths += length;
			Lengths->data.fl[i*num_lines + i] = (float)length;
					
			// Normalize into the sphere
			cvMatMul(Q,a,an);
			cvMatMul(Q,b,bn);

			double anorm = cvNorm(an, NULL, CV_L2);
			double bnorm = cvNorm(bn, NULL, CV_L2);
			cvScale(an,an,1/anorm);
			cvScale(bn,bn,1/bnorm);

			// Compute the general form of the line
			cvCrossProduct(an,bn,li);
			double linorm = cvNorm(li, NULL, CV_L2);
			cvScale(li,li,1/linorm);

			LSS->data.fl[i*3    ] = li->data.fl[0];
			LSS->data.fl[i*3 + 1] = li->data.fl[1];
			LSS->data.fl[i*3 + 2] = li->data.fl[2];			
		}	
		cvScale(Lengths,Lengths, 1/sum_lengths);
		cvMulTransposed(Lengths,Lengths,1);

		if (track && vp_prev_exists)
		{
			trackingMSAC(E, lines, num_vp);		
		}
		else
		{
#ifdef DEBUG_VERBOSE
			printf("RECOMPUTE VANISHING POINT");
#endif
			while ( (iter <= min_iters) || ((iter<=T_iter) && (iter <=max_iters) && (no_updates <= max_no_updates)) )
			{					
				iter++;

				if(iter >= min_iters)
					break;
				
				// Hypothesize -------------------------
				// Select MSS
				if(LSS->rows < (int)MSS.size())
					break;
				GetMinimalSampleSet(LSS, Lengths, MSS, v_est);

				// Test --------------------------------
				// Find the consensus set and cost	
				int N_I = 0;
				float J = GetConsensusSet(LSS, v_est, E, &N_I);		// the CS is indexed in CS_idx		

				// Update ------------------------------
				// If the new cost is better than the best one, update
				if (N_I >= minimal_sample_set_dimension && (J<J_best) || ((J == J_best) && (N_I > N_I_best)))
				{
					notify = true;
					
					J_best = J;
					cvCopy(v_est, v_best);
					cvCopy(CS_idx, CS_best);
						
					if (N_I > N_I_best)			
						update_T_iter = true;					

					N_I_best = N_I;

					if (update_T_iter)
					{
						// Update number of iterations
						double q = 0;
						if (minimal_sample_set_dimension > N_I_best)
						{
							// Error!
							perror("The number of inliers must be higher than minimal sample set");							
						}
						if(num_lines == N_I_best)
						{
							q = 1;
						}
						else
						{
							q = 1;
							for (int j=0; j<minimal_sample_set_dimension; j++)
								q *= (double)(N_I_best - j)/(double)(num_lines - j);
						}
						// Estimate the number of iterations for RANSAC
						if ((1-q) > 1e-12)
							T_iter = (int)ceil( log((double)epsilon) / log((double)(1-q)));
						else
							T_iter = 0;
					}
				}
				else
				{
					notify = false;
				}

				// Verbose ------------------------------
				if (verbose && notify)
				{
					printf("\nIteration = %5d/%9d. ", iter, T_iter);
					printf("Inliers = %6d/%6d (cost is J = %8.4f)", N_I_best, num_lines, J_best);
				}

				// Check CS length (for the case all line segments are in the CS)
				if (N_I_best == num_lines)	
					break;				
			}
		}

		// Reestimate ------------------------------
		if(verbose)
			printf("\nReestimating the solution... ");

		// Tengo que rellenar ind_CS_best, e.g. ind_CS_best = [12 13 15 129 132]; a partir del CS_idx
		//ind_CS_best = (int*) malloc(sizeof(int)*N_I_best);
		//int j= 0;
		for (int i=0; i<num_lines; i++)		
			if (CS_best->data.fl[i] == 1)							
				ind_CS_best.push_back(i);		

		if(ind_CS_best.size() == LSS->rows)
		{			
			EstimateVanishingPoint(LSS, Lengths, ind_CS_best, N_I_best, v_est);
			// Cost could be recomputed here...

			if(verbose)
			{
				printf("Done");
				printf("\nFinal number of inliers = %d/%d", N_I_best, num_lines); 
			}

			// Unnormalize and store
			if (v_est->rows == 1)
			{
				CvMat* v_estT = cvCreateMat(3,1,CV_32F);
				v_estT->data.fl[0] = v_est->data.fl[0];
				v_estT->data.fl[1] = v_est->data.fl[1];
				v_estT->data.fl[2] = v_est->data.fl[2];

				cvMatMul(Q_inv, v_estT, vanishing_point);
				
				if(verbose)
					printf("vn = (%.3f,%.3f,%.3f)\n", v_estT->data.fl[0], v_estT->data.fl[1], v_estT->data.fl[2]);
				cvReleaseMat(&v_estT);
			}
			else
			{
				cvMatMul(Q_inv, v_est, vanishing_point);
				if(verbose)
					printf("vn = (%.3f,%.3f,%.3f)\n", v_est->data.fl[0], v_est->data.fl[1], v_est->data.fl[2]);
			}	
			
			if(vanishing_point->data.fl[2] != 0)
			{
				vanishing_point->data.fl[0] /= vanishing_point->data.fl[2];
				vanishing_point->data.fl[1] /= vanishing_point->data.fl[2];
				vanishing_point->data.fl[2] = 1;
			}
			
			if(verbose)
				printf("v = (%.3f,%.3f,%.3f)\n", vanishing_point->data.fl[0], vanishing_point->data.fl[1], vanishing_point->data.fl[2]);
			
			
			// Copy a backup of the vanishing point			
			v_est_prev->data.fl[num_vp] = v_est->data.fl[0];			
			v_est_prev->data.fl[number_of_vps + num_vp] = v_est->data.fl[1];			
			v_est_prev->data.fl[number_of_vps*2 + num_vp] = v_est->data.fl[2];
		}
		//Release memory
		free(E);

		cvReleaseMat(&CS_best);
		cvReleaseMat(&CS_idx);
		cvReleaseMat(&LSS);
		cvReleaseMat(&a);
		cvReleaseMat(&b);
		cvReleaseMat(&an);
		cvReleaseMat(&bn);
		cvReleaseMat(&li);
		cvReleaseMat(&Lengths);

		*number_of_inliers = N_I_best;	
	}
}
void MSAC::singleVPEstimation(std::vector<std::vector<CvPoint> > &lines, int *number_of_inliers, CvMat *vanishing_point)
{
	int num_lines = lines.size();
std::cout<<"1"<<std::endl;
	// Fill vanishing_point with -1 if not enough num_lines
	if(num_lines < minimal_sample_set_dimension)
	{
		vanishing_point->data.fl[0] = -1;
		vanishing_point->data.fl[1] = -1;
		vanishing_point->data.fl[2] = -1;
	}
std::cout<<"2"<<std::endl;
	// Transform all line segments
	LSS = cvCreateMat(num_lines, 3, CV_32F);
	CvMat* a = cvCreateMat(3,1,CV_32F);
	CvMat* b = cvCreateMat(3,1,CV_32F);
	CvMat* an = cvCloneMat(a);
	CvMat* bn = cvCloneMat(b);
	CvMat* li = cvCreateMat(3,1,CV_32F);
std::cout<<"3"<<std::endl;
	// Loop over line segments		
	CvMat* Lengths = cvCreateMat(num_lines, num_lines, CV_32F);
	cvSetZero(Lengths);
	double sum_lengths = 0;
	for (int i=0; i<num_lines; i++)
	{
		// Extract the end-points					
		CvPoint p1 = lines[i][0];
		CvPoint p2 = lines[i][1];
		a->data.fl[0] = (float)p1.x;
		a->data.fl[1] = (float)p1.y;
		a->data.fl[2] = 1;
		b->data.fl[0] = (float)p2.x;
		b->data.fl[1] = (float)p2.y;
		b->data.fl[2] = 1;	

		double length = sqrt((b->data.fl[0]-a->data.fl[0])*(b->data.fl[0]-a->data.fl[0]) + (b->data.fl[1]-a->data.fl[1])*(b->data.fl[1]-a->data.fl[1]));
		sum_lengths += length;
		Lengths->data.fl[i*num_lines + i] = (float)length;
				
		// Normalize into the sphere
		cvMatMul(Q,a,an);
		cvMatMul(Q,b,bn);

		double anorm = cvNorm(an, NULL, CV_L2);
		double bnorm = cvNorm(bn, NULL, CV_L2);
		cvScale(an,an,1/anorm);
		cvScale(bn,bn,1/bnorm);

		// Compute the general form of the line
		cvCrossProduct(an,bn,li);
		double linorm = cvNorm(li, NULL, CV_L2);
		cvScale(li,li,1/linorm);

		LSS->data.fl[i*3    ] = li->data.fl[0];
		LSS->data.fl[i*3 + 1] = li->data.fl[1];
		LSS->data.fl[i*3 + 2] = li->data.fl[2];			
	}	
	cvScale(Lengths,Lengths, 1/sum_lengths);
	cvMulTransposed(Lengths,Lengths,1);
std::cout<<"4"<<std::endl;
	// Estimate the vanishing point
	std::vector<int> set;
	set = std::vector<int>(num_lines, 0);
	for(unsigned int i=0; i<num_lines; i++)
		set[i] = i;
std::cout<<"4.5"<<std::endl;
	EstimateVanishingPoint(LSS, Lengths, set, num_lines, vanishing_point);
	*number_of_inliers = num_lines;

std::cout<<"5"<<std::endl;
	// Unnormalize
	cvMatMul(Q_inv, vanishing_point, vanishing_point);

	if(vanishing_point->data.fl[2] != 0)
	{
		vanishing_point->data.fl[0] /= vanishing_point->data.fl[2];
		vanishing_point->data.fl[1] /= vanishing_point->data.fl[2];
		vanishing_point->data.fl[2] = 1;
	}
std::cout<<"6"<<std::endl;
	cvReleaseMat(&LSS);
	cvReleaseMat(&a);
	cvReleaseMat(&b);
	cvReleaseMat(&an);
	cvReleaseMat(&bn);
	cvReleaseMat(&li);
	cvReleaseMat(&Lengths);

}
void MSAC::GetMinimalSampleSet(CvMat* LSS, CvMat* Lengths, std::vector<int> &MSS, CvMat* v_est)
{	
	int N = LSS->rows;	

	// Generate a pair of samples
	while (N <= (MSS[0] = rand() / (RAND_MAX/(N-1))));
	while (N <= (MSS[1] = rand() / (RAND_MAX/(N-1))));

	// Estimate the vanishing point and the residual error
	EstimateVanishingPoint(LSS,Lengths, MSS, 2, v_est);		
}

void MSAC::EstimateVanishingPoint(CvMat* LSS, CvMat* Lengths, std::vector<int> &set, int set_length, CvMat* v_est)
{
	
std::cout<<"45.1"<<std::endl;
	if (set_length == minimal_sample_set_dimension)
	{	
		// Just the cross product
		CvMat* ls0 = cvCreateMat(3,1,CV_32F);
		CvMat* ls1 = cvCreateMat(3,1,CV_32F);

		CvMat* ls0_aux = cvCreateMat(1,3,CV_32F);
		CvMat* ls1_aux = cvCreateMat(1,3,CV_32F);		

		cvGetRow(LSS,ls0_aux,set[0]);
		cvGetRow(LSS,ls1_aux,set[1]);

		cvTranspose(ls0_aux, ls0);
		cvTranspose(ls1_aux, ls1);

		cvCrossProduct(ls0, ls1, v_est);
std::cout<<"45.2"<<std::endl;
		if (v_est->data.fl[2] !=0)
		{
			v_est->data.fl[0] /= v_est->data.fl[2];
			v_est->data.fl[1] /= v_est->data.fl[2];
			v_est->data.fl[2] = 1;
		}

		cvReleaseMat(&ls0);
		cvReleaseMat(&ls1);
		cvReleaseMat(&ls0_aux);
		cvReleaseMat(&ls1_aux);		
		return;
	}	
	else if (set_length<minimal_sample_set_dimension)
	{
		perror("Error: at least 2 line-segments are required\n");
		return;
	}
std::cout<<"45.3"<<std::endl;
	// Extract the line segments corresponding to the indexes contained in the set
	CvMat* LSS_set = cvCreateMat(3,set_length, CV_32F);//(set_length, 3);
	CvMat* Lengths_set = cvCreateMat(set_length, set_length, CV_32F);
	cvSetZero(Lengths_set);

	int total_width = Lengths->width;

	vp_length_ratio = 0;
	
	for (int i=0; i<set_length; i++)
	{
		LSS_set->data.fl[i               ] = LSS->data.fl[set[i]*3    ];
		LSS_set->data.fl[set_length   + i] = LSS->data.fl[set[i]*3 + 1];
		LSS_set->data.fl[set_length*2 + i] = LSS->data.fl[set[i]*3 + 2];

		Lengths_set->data.fl[set_length*i + i] = Lengths->data.fl[set[i]*total_width + set[i]];
		vp_length_ratio += sqrt(Lengths_set->data.fl[set_length*i + i]);
	}		

	// Generate the matrix ATA (a partir de LSS_set=A)
	CvMat* ATA = cvCreateMat(3,3, CV_32F);
	CvMat* Aux1 = cvCreateMat(3,set_length,CV_32F);
	CvMat* Aux2 = cvCreateMat(set_length, 3, CV_32F);
	cvMatMul(LSS_set, Lengths_set, Aux1);
	cvTranspose(LSS_set,Aux2);
	cvMatMul(Aux1,Aux2, ATA);
std::cout<<"45.4"<<std::endl;
	// Obtain eigendecomposition
	CvMat* eigenvals = cvCreateMat(3,1, CV_32F);
	CvMat* eigenvects = cvCreateMat(3,3, CV_32F);

	cvSVD(ATA, eigenvals, eigenvects, 0, CV_SVD_U_T + CV_SVD_MODIFY_A);

std::cout<<"45.4.1"<<std::endl;	
	// Assign the result	
	v_est->data.fl[0] = eigenvects->data.fl[6];
	v_est->data.fl[1] = eigenvects->data.fl[7];
	v_est->data.fl[2] = eigenvects->data.fl[8];
std::cout<<"45.4.2"<<std::endl;
	double vnorm = cvNorm(v_est, NULL, CV_L2);
	cvScale(v_est,v_est,1/vnorm);
std::cout<<"45.5"<<std::endl;
	// Unnormalize
	//cvMatMul(Q_inv, v_est, v_est); // es mejor trabajar con todo normalizado y desnormalizar al salir
	
	cvReleaseMat(&LSS_set);
	cvReleaseMat(&Lengths_set);
	cvReleaseMat(&ATA);
	cvReleaseMat(&Aux1);	
	cvReleaseMat(&Aux2);
	cvReleaseMat(&eigenvals);
	cvReleaseMat(&eigenvects);

	return;
}


float MSAC::GetConsensusSet(CvMat* LSS, CvMat* v_est, float* E, int* CS_counter)
{
	// Compute the error of each line segment of LSS with respect to v_est
	// If it is less than the threshold, add to the CS
	CvMat* vn = cvCreateMat(3,1,CV_32F);
	//cvMatMul(Q,v_est,vn);				// if normalizaed before
	cvCopy(v_est, vn);
	
	double vn_norm = cvNorm(vn, NULL, CV_L2);
	CvMat* li = cvCreateMat(3,1,CV_32F);
	CvMat* li_aux = cvCreateMat(1,3,CV_32F);
	
	double li_norm = 0;
	float di = 0;
	
	cvSetZero(CS_idx);

	float J = 0;

	for(int i=0; i<LSS->rows; i++)
	{
		cvGetRow(LSS, li_aux, i);
		cvTranspose(li_aux, li);
		li_norm = cvNorm(li, NULL, CV_L2); // esto lo podria precalcular antes
		di = (float)cvDotProduct(vn,li);
		di /= (float)(vn_norm*li_norm);

		E[i] = di*di;
		
		if (E[i] <= T_noise_squared)
		{
			CS_idx->data.fl[i] = 1;		// set index to 1
			(*CS_counter)++;
			
			// Torr method
			J += E[i];
		}
		else
		{
			J += T_noise_squared;
		}
	}	

	J /= *CS_counter;

	// If inside the image, penalyze
	// Unnormalize and store
	if (v_est->rows == 1)
	{
		CvMat* v_estT = cvCreateMat(3,1,CV_32F);
		v_estT->data.fl[0] = v_est->data.fl[0];
		v_estT->data.fl[1] = v_est->data.fl[1];
		v_estT->data.fl[2] = v_est->data.fl[2];

		cvMatMul(Q_inv, v_estT, vn);		// uso vn como auxiliar
		cvReleaseMat(&v_estT);

		vn->data.fl[0] /= vn->data.fl[2];
		vn->data.fl[1] /= vn->data.fl[2];
	}
	else
	{
		cvMatMul(Q_inv, v_est, vn);		
		vn->data.fl[0] /= vn->data.fl[2];
		vn->data.fl[1] /= vn->data.fl[2];
	}	

	if((vn->data.fl[0] > -this->width && vn->data.fl[0] < 2*this->width) && (vn->data.fl[1] > -this->height && vn->data.fl[1] < 2*this->height))
	{
		// Inside point!!
		J = 100;
	}

	// Release
	cvReleaseMat(&vn);
	cvReleaseMat(&li);
	cvReleaseMat(&li_aux);

	return J;
}
void MSAC::setTrack(bool track)
{
	this->track = track;
}
void MSAC::trackingMSAC(float* E, std::vector<std::vector<CvPoint> > &lines, int num_vp)
{
	// Don't select randomly. Instead, use previous estimation
	// 1.- Compute the error for all new data samples with respect to previous estimation
	float di = 0;			
	int MSS_1 = -1; double cost_1 = 1;
	int MSS_2 = -1; double cost_2 = 1;
	CvMat* li = cvCreateMat(3,1,CV_32F);
	CvMat* li_aux = cvCreateMat(1,3,CV_32F);
	CvMat* v_prev = cvCreateMat(3,1,CV_32F);

	v_prev->data.fl[0] = v_est_prev->data.fl[num_vp];
	v_prev->data.fl[1] = v_est_prev->data.fl[number_of_vps + num_vp];
	v_prev->data.fl[2] = v_est_prev->data.fl[2*number_of_vps + num_vp];

	// Compute the MSS as the two lsegs with lower error with respect to the predicted vp
	// By now the prediction follows a static model without noise
	for(int i=0; i<LSS->rows; i++)
	{
		cvGetRow(LSS, li_aux, i);
		cvTranspose(li_aux, li);
		double li_norm = cvNorm(li, NULL, CV_L2); // esto lo podria precalcular antes
		di = (float)cvDotProduct(v_prev,li);
		di /= (float)(li_norm);	// no hace falta dividir por la norma del v_est_prev, ya que ya esta normalizado de antes

		if (ABS(di) < cost_1)
		{
			// This one is the best one!
			// First, copy what was in the first into the second
			MSS_2 = MSS_1;
			cost_2 = cost_1;

			// Now update first
			MSS_1 = i;
			cost_1 = ABS(di);
		}
		else if(ABS(di) < cost_2)
		{
			// This is not the best, but it is best than the second
			MSS_2 = i;
			cost_2 = di;
		}
	}
	cvReleaseMat(&li);
	cvReleaseMat(&li_aux);
	cvReleaseMat(&v_prev);
	MSS[0] = MSS_1;
	MSS[1] = MSS_2;
	EstimateVanishingPoint(LSS,NULL,MSS,2,v_est);
				
	N_I_best = 0;
	J_best = GetConsensusSet(LSS, v_est, E, &N_I_best);		// the CS is indexed in CS_idx		
	notify = true;
	cvCopy(v_est, v_best);
	cvCopy(CS_idx, CS_best);
			
	// Verbose ------------------------------
	if (verbose && notify)
	{				
		printf("Inliers = %6d/%6d (cost is J = %8.4f)", N_I_best, lines.size(), J_best);
	}
}

int MSAC::getNumberVPS()
{
	return this->number_of_vps;
}