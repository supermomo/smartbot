#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include "HoughMethod/src/Ouf.h"

#define BAUDRATE B115200
#define DEVICE "/dev/ttyACM0"

int houghMethod(int fd);

/* void writeLog(int fd, const char* buff) { */
/*   write(fd, buff, sizeof(char)*(strlen(buff)+1)); */
/* } */

int main() 
{
  int usb = open (DEVICE, O_RDWR);
  struct termios tty; // Interface série
  int fileToRead = -1; // Fichier image le plus récent à lire
  int img = -1;
  int imgSent = 0;
  char buff;
  int lus;
 
  // Comm série via USB
  if (usb < 0)
  {
    perror ("Usb error");
    return 1;
  }
  tcgetattr(usb, &tty);

  cfsetospeed(&tty, BAUDRATE);
  cfsetispeed(&tty, BAUDRATE);
  
  tcsetattr(usb, TCSANOW, &tty);
  
  Ouf hough(usb);

  while(1) {
    fileToRead = hough.Ouf::houghSmartUpdate();
    //printf("%d.jpg\n", fileToRead);
    if((img == -1 || imgSent) && fileToRead != -1) {
      if(img != -1) {
	//close(img);
      }
      char* path;
      sprintf(path, "/home/pi/Detector/%d.jpg", fileToRead);
      //img = open(path, O_RDONLY);
      imgSent = 0;
    }
    
    for(int i = 0; i < 3000 && imgSent == 0; ++i) {
      printf("Lecture\n");
      if((lus = read(img, &buff, sizeof(char))) > 0) {
	write(usb, &buff, lus);
      } else {
	imgSent = 1;
      }
    }
  }
  return 0;
}
