# Makefile pour le projet SmartBot
# Auteurs : Gwenn Aubert, Amaury Boller, Matthieur Maugard, Bastien Maureille
# Version : 1.0

CC=g++
OPTION=-Wall
TARGET=GoBot
LIBS=-lopencv_core -lopencv_highgui -lopencv_objdetect -lopencv_imgproc -lopencv_video -lopencv_ml
BUILDDIR=./HoughMethod/Build/

GoBot: GoBot.c ./HoughMethod/src/Ouf.h ./HoughMethod/Build/Ouf.o
	$(CC) $(OPTION) -o GoBot GoBot.c $(BUILDDIR)Ouf.o $(BUILDDIR)MSAC2.o $(BUILDDIR)errorNIETO.o ./HoughMethod/src/lmmin.o $(LIBS)

./HoughMethod/Build/Ouf.o: ./HoughMethod/src/Ouf.cpp
	cd ./HoughMethod/; make Ouf.o

clean: 
	rm GoBot ./HoughMethod/Build/Ouf.o
