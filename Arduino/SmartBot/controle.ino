void parseCommande(byte com) {
  // Controle du servo
  // On reconstruis la valeur de l'angle (ou des commandes) à partir des 4 premiers bit
  int valueServo = bitRead(com, 7)*8;
  valueServo += bitRead(com, 6)*4;
  valueServo += bitRead(com, 5)*2;
  valueServo += bitRead(com, 4);
  
  // Ici on reconstruis la vitesse à partir des 3 derniers bit
  int valueMotor = bitRead(com, 2)*4;
  valueMotor += bitRead(com, 1)*2;
  valueMotor += bitRead(com, 0);
  
  //bitRead(com, 4) ? moteur1.run(BACKWARD);moteur2.run(BACKWARD) : moteur1.run(FORWARD);moteur2.run(FORWARD); 
  
  if(valueServo == 9) {
    turbo = true;
    timerTurbo = currentMillis;
  }
  
  if(valueMotor == 0 && !bitRead(com,3)) {
    
    if(valueServo == 8) {
      droiteDirec = droiteDirec - 2; // On se décale vers la gauche
    }
    else {
      if(valueServo != 0) {
        droiteDirec = droiteDirec + 2; // on se décale vers la droite
      }
    }
    direc.write(droiteDirec - 12);
  }
  else 
  {
    direc.write(droiteDirec - 3*valueServo);
  }
  
  //commandeServo
  
  
  //commandeMoteur
  if(!turbo) {
    
    if(bitRead(com,3)){
      digitalWrite(ledBack, HIGH); //led back on
      digitalWrite(ledFront, LOW); //led front off
      motor1.run(BACKWARD);
      motor2.run(BACKWARD);
      motor3.run(BACKWARD);
      motor4.run(BACKWARD);
    }
    else{
      digitalWrite(ledFront, HIGH); //led front on
      digitalWrite(ledBack, LOW); //led back off 
      motor1.run(FORWARD);
      motor2.run(FORWARD);
      motor3.run(FORWARD);
      motor4.run(FORWARD);
    }
    
    if(valueMotor == 0) {
      digitalWrite(ledFront, LOW);
      digitalWrite(ledBack, LOW);
        speedMotor(0);
     }
     else 
     {
       speedMotor(80 + 22*valueMotor);
     }
  }
  else { // TUUUUUUUURBOOO !
    digitalWrite(ledFront, HIGH);
    digitalWrite(ledBack, HIGH);
    motor1.run(FORWARD);
    motor2.run(FORWARD);
    motor3.run(FORWARD);
    motor4.run(FORWARD);
    speedMotor(255);
  }
}

void speedMotor(int value) {
  motor1.setSpeed(value);
  motor2.setSpeed(value);
  motor3.setSpeed(value);
  motor4.setSpeed(value);
}

void stopMotor() {
  motor1.run(BACKWARD);
  motor2.run(BACKWARD);
  motor3.run(BACKWARD);
  motor4.run(BACKWARD);
  speedMotor(255);
  delay(200);
  speedMotor(0);
}
