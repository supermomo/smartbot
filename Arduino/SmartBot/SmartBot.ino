#include <SoftwareSerial.h>
#include <AFMotor.h>
#include <Servo.h> 
#include <math.h>

// Déclaration du servo
Servo direc;

// Jaune Tx sur A0, Orange Rx sur A1

// Déclaration des différents moteur
AF_DCMotor motor3(1); // Arriére
AF_DCMotor motor4(2); // Arriére
AF_DCMotor motor1(3); // Avant Droite
AF_DCMotor motor2(4); // Avant Gauche

int bluetoothTx = A0; // Pin analogique 0
int bluetoothRx = A1; // Pin analogique 1

int droiteDirec = 101; // Angle le plus a droite du robot, réglage par trim

// option du turbo
long timerTurbo = 0; // Timer de la batterie, stocke le moment ou l'on a activé le turbo
long interval = 2000; // Le turbo est actif pendant 2 secondes
boolean turbo = false;
unsigned long currentMillis; // Stocke le temps actuel

long timerBlue = 0; //Timer du bluetooth, qui stocke le dernier temps de reception d'instruction. Au dela de 2s, le robot s'arrete pour raison de sécurité
long timerBatterie = 0; //Timer de la batterie, qui stocke le dernier temps d'envoie de l'état de la batterie

int ledFront = A3;
int ledBack = A4;
int ledFrontValue = 0;
int ledBackValue = 0;

SoftwareSerial bluetooth(bluetoothTx, bluetoothRx); // Creation d'une communication série sur les ports analogiques 0 et 1

int analogPin = A2;
int val = 0;


void setup()
{
  //Setup usb serial connection to computer
  Serial.begin(115200);
 
  //Setup Bluetooth serial connection to android
  bluetooth.begin(9600);
  bluetooth.print("AT+BAUD8");
  delay(100);
  bluetooth.begin(115200);
  direc.attach(9);
  direc.write(droiteDirec - 12);
  
   pinMode(ledFront, OUTPUT); 
   pinMode(ledBack, OUTPUT); 
}

void loop()
{
  
  currentMillis = millis();
  if(bluetooth.available())
  {
    timerBlue = currentMillis;
    byte com = (byte)bluetooth.read();
    parseCommande(com);
  }
  
  if(currentMillis - timerTurbo > interval) {
    turbo = false;
  }
  
  if(currentMillis - timerBlue > interval) {
    direc.write(droiteDirec - 12);
    speedMotor(0);
  }
  
  if(currentMillis - timerBatterie > 20000){ // On envoie les infos de la batterie toute les minutes
    timerBatterie = currentMillis;
    val = analogRead(analogPin);
    val = ((val - 768)/16);
    bluetooth.write(val);
  }  
}

  


