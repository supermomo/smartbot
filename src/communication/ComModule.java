package communication;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;

public class ComModule{

	private OutputStream mmOutputStream = null;
	private InputStream mmInputStream = null;
	private volatile int X = 0;
	private volatile int Y = 0;
	private volatile int maxSpeedLength;
	private volatile boolean stopThread;

	private int lastSentAngle = -777;
	private int lastSentSpeed = -777;
	private ByteArrayOutputStream partialImage;
	
	private volatile boolean trimRight = false;
	private volatile boolean trimLeft = false;
	private volatile boolean trimOk = false;
	private volatile boolean trimOn = false;
	private volatile boolean startTurbo = false;
	private long lastSendingTime = 0;
	
	private int bateryLevel = 25;
	private boolean batteryCode = false;
	private int cptByte = 0;
	private boolean zeroDToCheck = false;
	
	private int imgWidth = 1600;
	private int imgHeight = 900;
	private int currentPixel = 0;
	private boolean newImageAvailable = false;
	private boolean currentImage = false;
	private boolean debImage = false;
	private boolean finImage = false;
	private boolean imageComplete = false;
	private volatile boolean smartModeOn = false;
	
	public ComModule(OutputStream out, InputStream in, int width, int height){
		mmOutputStream = out;
		mmInputStream = in;
		imgWidth = width;
		imgHeight = height;
		partialImage = new ByteArrayOutputStream();
		//partialImage = new char[imgWidth * imgHeight][3];
	}

	private class OutputModule extends Thread{
		
		@Override
		public void run(){

			while(!stopThread){
				//System.out.println("right tr : "+trimRight+" left tr +"+trimLeft);
				//System.out.println("out running");
				if(smartModeOn){
					System.out.println("smart bot toogledddddd");
					sendByte((char)160);
					smartModeOn = false;
				}else if(trimOk){
					sendTrimOk();
					trimOk = false;
				} else if (trimRight){
					sendTrimRight();
					trimRight = false;
				}
				else if (trimLeft){
					sendTrimLeft();
					trimLeft = false;
				}
				else if (startTurbo) {
					startTurbo();
					startTurbo = false;
				}
				else if(!trimOn) {
					sendMovementData();
				}
				
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		
	}
	
	private class InputModule extends Thread{
		
		@Override
		public void run(){
			while(!stopThread){
				System.out.println("in running");
				try {
					receiveData();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	
	}


	public void trimRight(){
		System.out.println("trim right");
		trimRight = true;
		trimOn = true;
	}

	public void trimLeft(){
		System.out.println("trim left");
		trimLeft = true;
		trimOn = true;
	}

	public void trimOk(){
		trimOk = true;
		trimOn = false;
	}

	public void turbo(){
		startTurbo = true;
	}

	private void startTurbo(){
		char res = 0x00;
		res += 9 * 16;
		sendByte(res);
		System.out.println("turbo");
	}
	
	private void sendTrimRight(){

		int firstHex = 1;

		int secondHex = 0;

		char res = 0x00;
		res += secondHex + firstHex * 16;

		sendByte(res);
	}

	private void sendTrimLeft(){

		int firstHex = 1 * 8;

		int secondHex = 0;

		char res = 0x00;
		res += secondHex + firstHex * 16;
		
		sendByte(res);
	}

	private void sendTrimOk(){

		char res = 0x00;
		
		sendByte(res);
	}

	private void sendByte(char data){

		try{
			System.out.println("Send: "+(int)data);
			mmOutputStream.write(data);
		} catch (Exception e){
			System.out.println("data sending failed");
		}

	}
	
	private void sendMovementData(){

		float angleTemp = (float)Math.atan2((float)Y, X );

		float len = (float)Math.sqrt((float)(X*X + Y*Y));

		//Normalize len beetwen -7 and 7
		len = len/(maxSpeedLength)*7;

		char message;

		if(X != 0 || Y != 0){
			message = convertToByte(angleTemp, len);
		} else {
			message = convertToByte((float)( -1.0 * Math.PI/2), -666.0f);
		}

		if(message != 0x00){
			sendByte(message);
		} else {
			//System.out.println("not sending");
		}
	}

	private void receiveData() throws InterruptedException{
		//TO IMPROVE
		byte[] dataArray = new byte[1024*3];
		System.out.println("receive data");
		try {
			int nbRead = mmInputStream.read(dataArray);
			//System.out.println("reading : " + nbRead);
			int cpt = 0;
			
			while(cpt<nbRead){
				
				byte res = (byte) dataArray[cpt];
				//4 first bytes
				byte code = (byte) (res/16);
				//System.out.println("Res: "+ (int)res);
				
				if(batteryCode && code == 0x00){
					//4 last bytes
					char value = (char) (res%16);
					bateryLevel = (int) (value/15.0f * 100.0f);
					//System.out.println("Battery data received : " + bateryLevel);
					
				}
				
				if(zeroDToCheck){
					if(res == 10){
						partialImage.write(res);
					}else{
						partialImage.write((byte)13);
						partialImage.write(res);
					}
					zeroDToCheck = false;
				}
				
				else if(res == 13 && currentImage){
					zeroDToCheck = true;
				}
				else if(res == -1) {//0xFF
					if(currentImage) {
						finImage = true;
						cptByte ++;
						partialImage.write(res);
					}
					else {
						debImage = true;
						batteryCode = true;
					}
					
					
				}
				else if(res == -40 && debImage) {//-40 = 0xD8
					cptByte ++;
					partialImage.write((byte)-1);
					cptByte ++;
					partialImage.write(res);
					currentImage = true;
					debImage = false;
					//System.out.println("///////////DEBUT IMAGE ///////////");
				}
				else if (res == -39 && finImage) {//-39 = 0xD9
					cptByte ++;
					partialImage.write(res);
					currentImage = false;
					finImage = false;
					//System.out.println("///////////FIN IMAGE ("+cptByte+" Bytes)///////////");
					imageComplete = true;
				}
				else if(currentImage){
					cptByte ++;
					partialImage.write(res);
					finImage = false;
					debImage = false;
					
				}
				else {
					System.out.print(" - "+res);
				}			
				cpt++;
			}
			System.out.println("FIN TRANSMISSION");
			//lastFullImage = partialImage;
			if(imageComplete) {
				imageComplete = false;
				try {
					
					String fileName = "/sdcard/stream.jpg";
					File dest = new File(fileName);
					dest.createNewFile();
					ByteArrayInputStream sourceFile = new ByteArrayInputStream(partialImage.toByteArray());
	
					try {
					FileOutputStream destinationFile = new FileOutputStream(dest);
	
					try {
					byte buffer[]=new byte[512*1024];
					int nbLecture;
	
					while( (nbLecture = sourceFile.read(buffer)) != -1 ) {
					destinationFile.write(buffer, 0, nbLecture);
					}
					} finally { destinationFile.close(); }
					} finally { sourceFile.close(); }
					} catch (IOException e) {
					e.printStackTrace();
					}
					cptByte = 0;
					partialImage.reset();
			}

			
				
			}catch (IOException e) {
				e.printStackTrace();
				} 
	}
				//if(res != 10){
					/*if(currentPixel/3 < imgWidth * imgHeight){
						//System.out.println("res is : "+res);
						partialImage[currentPixel/3][currentPixel%3] = res;
						//System.out.println("i : "+currentPixel/3+" j : "+currentPixel%3);
						currentPixel ++;
					} else {
						System.out.println("fin///////////////////////////////////////////");
						System.out.println(currentPixel/3 + " pixels  Fin image////////////////////////////////////////////////");
						lastFullImage = copy(partialImage, imgWidth * imgHeight, 3);
						partialImage = new char[imgWidth * imgHeight][3];
						fillWhite(partialImage, imgHeight * imgHeight);
						newImageAvailable = true;
						currentPixel = 0;
						//System.out.println("out of bound pixel size");
					}*/
				//}

				//}

	private char convertToByte(float angle, float speed){

		int firstHex = (int) Math.round(angle * (1/(-1 * Math.PI)) * 8);
		//System.out.println("angle " + firstHex);

		if(firstHex < 0){
			firstHex = Math.abs(firstHex); // angle
			speed = -speed;
		}
		//System.out.println("angle " + firstHex);

		int secondHex = Math.round(speed); // speed
		//System.out.println("speed : " + secondHex);

		char res = 0x00;

		if(speed != -666){
			//System.out.println("moving");
			res += Math.abs(secondHex);
			res += secondHex >= 0 ? 0 : 1 * 8;
		} else {//if we should not move
			//System.out.println("not moving");
			res += 1 * 8;
		}

		if(secondHex == lastSentSpeed && firstHex == lastSentAngle && System.currentTimeMillis()  < lastSendingTime + 1000){
			res = 0x00;
		} else {
			lastSentAngle = firstHex;
			lastSentSpeed = secondHex;
			res += firstHex * 16;
			lastSendingTime = System.currentTimeMillis();
			//System.out.println("maj timer");
		}

		return res;
	}

	
	private void fillWhite(char[][] tab, int size){
		
		for(int i = 0; i < size; ++i){
			
			for(int c = 0; c < 3; ++c){
				
				tab[i][c] = 0;
				
			}
			
		}
		
	}
	
	private char[][] copy(char[][] tab2, int size, int depth){
		
		char[][] res = new char[size][depth];
		
		for(int i = 0; i < size; ++i){
			
			for(int c = 0; c < depth; ++c){
				
				res[i][c] = tab2[i][c];
				
			}
			
		}
		return res;
		
	}
	
	public void toggleSmartMode() {
		System.out.println("tooogleiiiiiinnnng");
		smartModeOn = true;
    }

	public int getX() {
		return X;
	}


	public void setX(int x) {
		X = x;
	}


	public int getY() {
		return Y;
	}


	public void setY(int y) {
		Y = y;
	}


	public int getMaxSpeedLength() {
		return maxSpeedLength;
	}


	public void setMaxSpeedLength(int maxSpeedLength) {
		this.maxSpeedLength = maxSpeedLength;
	}


	public void stopThreads() {
		this.stopThread = true;
		System.out.println("threads stopped");
	}
	
	public void start(){
		this.stopThread = false;
		OutputModule outMod = new OutputModule();
		InputModule inMod = new InputModule();
		inMod.start();
		outMod.start();
		
		System.out.println("threads started");
	}
	

	public int getBateryLevel() {
		return bateryLevel;
	}

	public int getImgWidth() {
		return imgWidth;
	}

	public int getImgHeight() {
		return imgHeight;
	}

	public boolean isNewImageAvailable() {
		return newImageAvailable;
	}
	
	

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ComModule mod = new ComModule(null, null, 800, 600);
		mod.start();
		mod.turbo();
	}
}
