#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define BAUDRATE B115200
#define DEVICE "/dev/ttyAMA0"

void houghMethod(int fd);

int main()
{
  int fd;
  struct termios tty;

  /* Ouverture du device */
  if ((fd = open (DEVICE, O_RDWR)) < 0) {
    perror (DEVICE);
    return 1;
  }

  /* Lecture des paramètres */
  tcgetattr(fd, &tty);

  /* On passe la vitesse à 0 ==> hangup */
  cfsetospeed(&tty, BAUDRATE);
  cfsetispeed(&tty, BAUDRATE);

  /* On applique le nouveau paramètrage pendant 1s */
  tcsetattr(fd, TCSANOW, &tty);
  
  int img = open("/home/pi/Detector/img.jpg", O_RDONLY);
  char buff;
  int cpt = 0;
  int lus = 0;
  while((lus = read(img, &buff, sizeof(char))) > 0) {
    cpt++;
    write(fd, &buff, lus);
  }
  printf("\nOctets envoyés : %d\n", cpt);
  close(img);
  close(fd);
  return 0;
}
